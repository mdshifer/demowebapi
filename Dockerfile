FROM microsoft/dotnet:2.1-sdk AS builder  
WORKDIR /app

#Copy project from the source to the docker container filesystem 
#COPY DemoWebAPI/*.csproj ./DemoWebAPI/


#Copy all remaining files to our image
#COPY . .

#Install all Dependencies
#RUN dotnet restore DemoWebAPI/
COPY DemoWebAPI/*.csproj ./
RUN dotnet restore

COPY . .



#Publish our application
#RUN dotnet publish DemoWebAPI/DemoWebAPI.csproj -c Release -o /app/out

#RUN dotnet publish "DemoWebAPI/DemoWebAPI.csproj" --output "../app/out" --configuration Release --no-restore

RUN dotnet publish --output /app/ --configuration Release

#Runtime stage
FROM microsoft/dotnet:2.1-aspnetcore-runtime  


EXPOSE 80 

WORKDIR /app
COPY --from=builder /app .

#Entry point
ENTRYPOINT [ "dotnet" ]
CMD [ "DemoWebAPI.dll" ]
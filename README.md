**Dockerized ASP.NET CORE WEB API and Push it to ECS->EC2 **

This will help to get started with dockerizing your bitbucket repo and deploy it to AWS.

---

## Install and set up Docker machine and AWS CLI. 

You'll start by installing Docker if you don't already have it so you can run the docker image locally. And, You’ll need to have aws cli so you can
authenticate the docker login to the aws resources. I will only give you information about Window's docker installation. You can directly load Docker
work station form Dokers Hub if you have Windows Enterprise edition. It does not work for the home edition so we're going to install virtual machine or turn
on the Hypervisory to install open source docker-machine. 

1. I followed this link, https://www.sitepoint.com/docker-windows-10-home/, to install. Chocolatey makes things easier but you can choose other ways to install it. 
Ignore the parts where it tries to do Networking setup. 
Only open port 80/tcp and 22/tcp
2. Install AWS CLI Version 2: https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-windows.html#cliv2-windows-install 
3. Configure your aws environment. Command: aws configure
	* https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html
4. Make sure your docker-machine can be started and managed from linux command shells such us Git Bash. 
Open your git bash or Cygwin and source the docker. Cygwin is my choice of CLI. 
variables. 
	* docker-machine env default --shell linux
	And
	* eval $(docker-machine env default --shell linux)
5. Create repository on Elastic Container Service and grab all the instruction to do the image registry and push to update the services. 
This is the technically the circle CI part. This aws can also be used on Bitbucket pipline to sign in and do the same thing we do locally. 

---

## Building and Pushing docker image

Next, you’ll need to push the image to ECS repository.

1. Retrieve an authentication token and authenticate your Docker client to your registry.
The X marked part is the number that identifies your repository in ECS. 
Use the AWS CLI:
aws ecr get-login-password --region us-east-2 | docker login --username AWS --password-stdin XXXXXXXXXX.dkr.ecr.us-east-2.amazonaws.com/demowebapi

2. Build your Docker image using the following command. 
docker build -t imageName .
Example: docker build -t demowebapi .

3. After the build completes, tag your image so you can push the image to this repository:
docker tag demowebapi:latest XXXXXXXXXX.dkr.ecr.us-east-2.amazonaws.com/demowebapi:latest

4. Run the following command to push this image to your newly created AWS repository:
docker push XXXXXXXXXX.dkr.ecr.us-east-2.amazonaws.com/demowebapi:latest

5. Go and check your repository and the event log. 

---

## Create Service and Task Definition on top of the Cluster. 

We can use automated way of creating cluster 
https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ECS_AWSCLI_EC2.html
But you can also head to the AWS console and set it up manually so you can see what's actually happening. 

1. Go to Cluster -> Task Definitions -> Create new Task Definition -> Ener task Definition name -> give it a Task Role-> and Add container
2. Select EC2 and configure it in a way to grab the image from the repository
	Give a name to your container -> copy and past the repository URI in Image* box and add :latest to the end. 
	Give it a memory limit of 128. 
	Give it port: 80 and 80 in both boxes (you can add 443 as long as your local is also configured for https)
3. Asign a Task Definition that you just created to the Service with launch Type EC2.
4. Name your service and keep clicking next since we are not setting up load balancer and adjust the service. 
5. Create your service. 

## Required Environmental Variables - Goes under pipelines deployments 
   -  Go to Settings and find -> ../admin/addon/admin/pipelines/deployment-settings
    
	Enter these variables and their values 
	
	* AWS_ACCESS_KEY_ID : Your AWS access key.

	* AWS_SECRET_ACCESS_KEY : Your AWS secret access key. 

	* AWS_DEFAULT_REGION :  The AWS region code (us-east-1, us-west-2, etc.) of the region containing the AWS resource(s). 

## Bitbucket Pipeline 

Note yet to be added. But the build in pipeline are included and it should work. 

Run the same command you run to push the local docker image 

1.  Retrieve an authentication token and authenticate your Docker client to your registry.
	Use the AWS CLI:
	aws ecr get-login-password --region us-east-2 | docker login --username AWS --password-stdin xxxxxxxxxxxx.dkr.ecr.us-east-2.amazonaws.com/demowebapi
2.  Build your Docker image using the following command. For information on building a Docker file from scratch see the instructions here . 
	You can skip this step if your image is already built:
	docker build -t demowebapi .
3.	After the build completes, tag your image so you can push the image to this repository:
	docker tag demowebapi:latest xxxxxxxxxxxx.dkr.ecr.us-east-2.amazonaws.com/demowebapi:latest
	
4. Run the following command to push this image to your newly created AWS repository:
	docker push xxxxxxxxxxxx.dkr.ecr.us-east-2.amazonaws.com/demowebapi:latest
	
## Gotchas

1. Docker build fails in old SDK versions.
	
	For older frameworks: you need to add <GenerateProgramFile> as false. 
	
<!-- 	<PropertyGroup>
		<TargetFramework>netcoreapp2.1</TargetFramework>
		<IsPackable>false</IsPackable>
		<GenerateProgramFile>false</GenerateProgramFile>
	</PropertyGroup> -->

2. You may run into problem with docker commands
	
	Set the environment variables using these commands: 
	 First make sure you create your vbox
	 docker-machine create --driver virtualbox default
	 * This works as long as you have created virtubal box known as default 
	
		- $ docker-machine env default --shell linux
			
			export DOCKER_TLS_VERIFY="1"
			export DOCKER_HOST="tcp://192.168.99.101:2376"
			export DOCKER_CERT_PATH="C:\Users\mikes\.docker\machine\machines\default"
			export DOCKER_MACHINE_NAME="default"
			export COMPOSE_CONVERT_WINDOWS_PATHS="true"
	# Run this command to configure your shell:
	
	 eval $("C:\ProgramData\chocolatey\lib\docker-machine\bin\docker-machine.exe" env default --shell linux)

	mikes@LAPTOP- /cygdrive/c/Users/mikes/demowebapi
	$ eval $(docker-machine env default --shell linux)





